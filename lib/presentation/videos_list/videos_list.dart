import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

import '../video_item/video_item.dart';

class VideosList extends StatefulWidget {
  @override
  _VideosListState createState() => _VideosListState();
}

class _VideosListState extends State<VideosList> {
  List<dynamic> videos = [];

  @override
  void initState() {
    super.initState();
    _getVideosList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CupertinoNavigationBar(
        middle: Text('Videos List'),
        trailing: GestureDetector(
          child: Icon(CupertinoIcons.add),
          onTap: () {
            _captureVideo();
          },
        ),
      ),
      body: Container(
        child: videos.length == 0
            ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.videocam_off),
                    Text('Videos list is empty'),
                  ],
                ),
              )
            : GridView.count(
                crossAxisCount: 2,
                children: List.generate(
                    videos.length, (index) => _videoItemCard(index)),
              ),
      ),
    );
  }

  _captureVideo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    File video = await ImagePicker.pickVideo(
      source: ImageSource.camera,
    );

    if (video != null) {
      DateTime creationTime = video.lastModifiedSync();
      List<String> videoItem = [
        video.path,
        'Снято ${creationTime.day}/${creationTime.month}/${creationTime.year} в ${creationTime.hour}:${creationTime.minute}'
      ];
      print('Video saved succesfull at ' + video.path);
      prefs.setStringList('video_' + Uuid().v4(), videoItem);
      setState(() {
        videos.add(videoItem);
      });
    }
  }

  _getVideosList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Set<String> keys = prefs.getKeys();
    var files = [];
    for (var key in keys) {
      files.add(prefs.getStringList(key));
    }
    setState(() {
      videos = files;
    });
  }

  _videoItemCard(int index) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => VideoItem(
                  filepath: videos[index][0],
                ),
          ),
        );
      },
      child: Card(
        child: Column(
          children: <Widget>[
            Expanded(
              child: Center(
                child: Icon(Icons.videocam, size: 64.0, color: Colors.green,),
              ),
            ),
            Text(videos[index][1]),
          ],
        ),
      ),
    );
  }
}
