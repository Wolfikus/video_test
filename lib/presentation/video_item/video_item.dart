import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:video_player/video_player.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart';
import 'package:flutter_ffmpeg/flutter_ffmpeg.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:share_extend/share_extend.dart';

class VideoItem extends StatefulWidget {
  final String filepath;

  VideoItem({this.filepath});

  _VideoItemState createState() => _VideoItemState();
}

class _VideoItemState extends State<VideoItem> {
  FlutterFFmpeg _flutterFFmpeg = FlutterFFmpeg();
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;
  File currentVideo;

  Duration _trimStart;
  Duration _trimEnd;
  bool isTrimming;

  @override
  void initState() {
    _controller = VideoPlayerController.file(_getFile());

    _initializeVideoPlayerFuture = _controller.initialize().then((_) {
      _trimStart = Duration.zero;
      _trimEnd = _controller.value.duration;
      isTrimming = false;
    });

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CupertinoNavigationBar(
          middle: Text('Video Item'),
        ),
        body: Container(
          color: Colors.black,
          child: FutureBuilder(
            future: _initializeVideoPlayerFuture,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return Stack(
                  alignment: AlignmentDirectional.bottomCenter,
                  children: <Widget>[
                    Center(
                      child: AspectRatio(
                        aspectRatio: _controller.value.aspectRatio,
                        child: VideoPlayer(_controller),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        RaisedButton(
                          onPressed: () {
                            setState(() {
                              if (_controller.value.isPlaying) {
                                _controller.pause();
                              } else {
                                _controller.play();
                              }
                            });
                          },
                          child: Icon(
                            _controller.value.isPlaying &&
                                    _controller.value.position !=
                                        _controller.value.duration
                                ? Icons.pause
                                : Icons.play_arrow,
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        RaisedButton(
                            onPressed: () async {
                              await showCupertinoModalPopup(
                                context: context,
                                builder: (context) => Container(
                                      color: Colors.white,
                                      padding:
                                          EdgeInsets.symmetric(vertical: 15.0),
                                      width: MediaQuery.of(context).size.width,
                                      height: 175.0,
                                      child: Column(
                                        children: <Widget>[
                                          Text('Cut it'),
                                          RangeSlider(
                                            upperValue: _trimEnd.inMilliseconds
                                                .toDouble(),
                                            lowerValue: _trimStart
                                                .inMilliseconds
                                                .toDouble(),
                                            max: _controller
                                                .value.duration.inMilliseconds
                                                .toDouble(),
                                            min: 0.0,
                                            onChanged: (double newLowerValue,
                                                double newUpperValue) {
                                              setState(() {
                                                _trimStart = Duration(
                                                    milliseconds:
                                                        newLowerValue.toInt());
                                                _trimEnd = Duration(
                                                    milliseconds:
                                                        newUpperValue.toInt());
                                              });
                                            },
                                          ),
                                          isTrimming
                                              ? CupertinoActivityIndicator()
                                              : CupertinoButton(
                                                  child: Text('Cut and Share'),
                                                  onPressed: () {
                                                    _cut();
                                                  },
                                                ),
                                        ],
                                      ),
                                    ),
                              );
                            },
                            child: Icon(
                              Icons.content_cut,
                            )),
                      ],
                    ),
                  ],
                );
              } else {
                return Center(child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.white),));
              }
            },
          ),
        ));
  }

  File _getFile() {
    return File(widget.filepath);
  }

  _cut() async {
    setState(() {
      isTrimming = true;
    });
    Directory tempDir = await getTemporaryDirectory();
    String outputPath = "${tempDir.path}/${basename(widget.filepath)}";
    String duration = (_trimEnd - _trimStart).toString();
    String command =
        '-i ${widget.filepath} -ss ${_trimStart.toString()} -t $duration $outputPath';
    print('Executable command ' + command);
    await _flutterFFmpeg.execute(command).then((_) {
      setState(() {
        isTrimming = false;
      });
    });
    await ShareExtend.share(outputPath, 'file');
  }
}
