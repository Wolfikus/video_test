import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import './presentation/videos_list/videos_list.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      title: 'Video Editor Test',
      home: VideosList(),
      debugShowCheckedModeBanner: false,  
    );
  }
}