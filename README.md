# flutter-video-test

This is the example of small application, built without any state management practices for video trimming and sharing on-the-go.

### Installing

Just clone the repository and use `fluter run`

## Built With

* [path](https://pub.flutter-io.cn/packages/path) - Used to get filenames
* [path_provider](https://pub.flutter-io.cn/packages/path_provider) - Used to get temporary directory path based on platform
* [image_picker](https://pub.flutter-io.cn/packages/image_picker) - Video capturing and saving
* [shared_preferences](https://pub.flutter-io.cn/packages/shared_preferences) - For access and modifying internal storage system of device
* [video_player](https://pub.flutter-io.cn/packages/video_player) - Use to play video and get its' properties (ex. duration)
* [uuid](https://pub.flutter-io.cn/packages/uuid) - Generating video ids
* [share_extend](https://pub.flutter-io.cn/packages/share_extend) - Share the result of trimming
* [flutter_ffmpeg](https://pub.flutter-io.cn/packages/flutter_ffmpeg) - Video trimming and adjusting bitrate parameters
* [flutter_range_slider](https://pub.flutter-io.cn/packages/flutter_range_slider) - Slider for visual video trimming values setup

